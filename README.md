*This set of documents was unanimously ratified by the RedWarn team in merge request !1 and went into force on January 11, 2021.*

![RedWarn logo](https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/RedWarn_Wordmark_%28for_bright_backgrounds%29.svg/2560px-RedWarn_Wordmark_%28for_bright_backgrounds%29.svg.png)
The RedWarn organization (also known as the RedWarn team) is a group of Wikipedia editors who develop the counter-vandalism tool RedWarn. We are not a legally-registered organization, nor are we an incorporated entity, bit simply a group of editors working towards one goal: the improvement of the existing anti-vandalism toolset to help the Wikimedia movement.

Although we're just a group of editors, there's no denying that we have a big responsibility in keeping RedWarn (the tool) updated and in shape. Because of that, we've set a few ground rules when it comes to maintaining RedWarn with the hope that even if an important editor goes missing, RedWarn can still run smoothly and improved by a new team.

This repository is public and open-source for complete transparency. It'll also allow other editors who may not be in the team to make changes and pull requests which essentially act as policy changes. We hope that with these guidelines in place, RedWarn's operation will remain smooth and in favor of open-source.

# Sections
1. [Foreword](/1_FOREWORD.md)
2. [Definition of Terms](/2_DEFINITION.md)
3. [Organizational Structure](/3_STRUCTURE.md)
4. [Code of Conduct](/4_CODE_OF_CONDUCT.md)
5. [Infrastructure](/5_INFRASTRUCTURE.md)
6. [Legal Notes](/6_LEGAL.md)
7. [Licensing](/7_LICENSING.md)

# License
These terms are licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported License. A copy of this license can be found at [`LICENSE`](/LICENSE).
