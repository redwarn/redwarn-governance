⟪ [Back](/README.md)
# Infrastructure
The RedWarn team relies on the following infrastructure in order to properly operate the userscript. This page documents existing and former infrastructure components used to operate RedWarn team projects.

## Toolforge
The RedWarn team operates a Toolforge tool under the ID [`redwarn`](https://toolsadmin.wikimedia.org/tools/id/redwarn). This tool contains content delivery services and RedWarn Tools, a legacy PHP-based interface for determining the English Wikipedia's reverts per minute (RPM).

The project lead and assistant lead are given maintainer access to the Toolforge tool and are responsible for its upkeep. Additional maintainers may be added at their discretion or at the discretion of the team. Removal of maintainers may only be done with a majority vote, or if the maintainer leaves by their own discretion.

## RedWarn Runner
The RedWarn organization GitLab Runner used to operate on the Virtual Private Server (VPS) provided by the Wikimedia Foundation (through the Wikimedia Cloud Services team). This runner is no longer available and was decommissioned when the ownership of the VPS was transferred to Ed Englefield.

## RedWarn.org
RedWarn.org was an old domain registered by Ed Englefield prior to leaving the RedWarn team. This domain is no longer maintained nor renewed by the incumbent RedWarn team.