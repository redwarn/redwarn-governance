⟪ [Back](/README.md)
# Foreword
RedWarn and the RedWarn team will forever be dedicated to creating and developing open-source software for all Wikimedians. As part of the [Wikimedia movement](https://en.wikipedia.org/wiki/Wikimedia_movement), we commit to making wiki anti-vandalism tools freely usable and accessible for everyone.

Although a minimal amount of bureaucracy would be beneficial, we acknowledge that our roles as developers require a level of order for RedWarn users to have the best experience possible. We aim not to be bound by excessive legalese and policy.

We hope that these policies can help guide the path that the RedWarn project takes not just in the next few years, but for the inevitable future.