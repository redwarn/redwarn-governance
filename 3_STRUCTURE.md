⟪ [Back](/README.md)
# Organizational Structure
The RedWarn team is composed of its members, headed by the project lead and assistant project lead. The lead positions are put into place as an order of succession. Their roles are defined below:

## Project Lead
The RedWarn team project lead is in charge of managing the team. They are the primary contact for anything involving RedWarn, such as community discussions, organizational emails, ownership of project resources, and legal registrations. They are given owner-level access to the primary RedWarn communication channel (as of now, the RedWarn Discord Server).

## Assistant Project Lead
In the event that the RedWarn team project lead is no longer available, the Assistant Project Lead will succeed to the position of Project Lead. They are expected to assist the Project Lead in organization-level tasks.

## Removal of roles
The Project Lead or Assistant Project Lead may be removed with a vote of no confidence consisting of a majority vote. If the Project Lead is removed, the Assistant Project Lead will immediately succeed the role. If the Assistant Project Lead is removed, another one should be chosen.

## Assigning of roles
The Assistant Project Lead is theoretically the only position that needs discussion (as the Project Lead position is granted by succession). For the Assistant Project Lead to be assigned, the team must settle on a consensus, with the Project Lead acting as a non-voting party and eventual arbitrator.

## Other roles
Other managerial roles only represent a member's tasks within the organization. All members are to be treated equally.