⟪ [Back](/README.md)
# Legal Notes
The RedWarn team project lead is responsible as the point of contact for any legal matters pertaining to RedWarn.

## Terms of Use
Being a userscript located on Wikimedia Foundation wikis, the RedWarn userscript is bound by the [Wikimedia Foundation Terms of Use](https://foundation.wikimedia.org/wiki/Terms_of_Use). Additional restrictions are applied on the RedWarn Toolforge tool, namely the [Wikimedia Cloud Services Terms of Use](https://wikitech.wikimedia.org/wiki/Wikitech:Cloud_Services_Terms_of_use) and the [Toolforge rules](https://wikitech.wikimedia.org/wiki/Help:Toolforge/Rules).

## Privacy Policy
Connections to Wikimedia Foundation servers, such as those involved in downloading the userscript from Wikipedia and in accessing the RedWarn Toolforge webservice, are subject to the [Wikimedia Foundation Privacy Policy](https://foundation.wikimedia.org/wiki/Privacy_policy). RedWarn team repostiories are hosted on GitLab, Inc. servers, which are subject to the [GitLab Privacy Policy](https://about.gitlab.com/privacy/).

Privacy concerns may arise whenever you choose to send personal information to RedWarn team members for error reporting. Personal information shared with team members must be kept confidential and accessible only to that specific team member unless explicit permission has been given by the sender.

RedWarn team members will never ask for login credentials, such as passwords or two-factor authentication numbers. If you have evidence of a RedWarn team member being intrusive on your personal information, please send an email to [`redwarn.issues@toolforge.org`](mailto:redwarn.issues@toolforge.org) with pertinent details.

## Naming
Due to vague trademark restrictions, the RedWarn team is currently bound by a Name License initiated by Ed Englefield prior to the transfer. This agreement can be found on [Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/c/c7/RedWarn_Transfer.pdf). The incumbent project lead, Chlod Alejandro, being the signatory of this agreement, is the sole point of contact for concerns regarding the agreement.

Further contact regarding the agreement, including complaints and/or legal concerns, must be made on the [English Wikipedia talk page](https://en.wikipedia.org/wiki/User_talk:Chlod) of the project lead. Attempts at discussion regarding the agreement through other channels will be ignored.
