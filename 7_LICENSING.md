⟪ [Back](/README.md)
# Licensing
RedWarn team projects are subject to free and open-source licenses. Please see the `LICENSE` file for the respective RedWarn team repository for more information.

By contributing to RedWarn team repositories, you agree to license your contributions under the license of that repository.

## RedWarn Web
The RedWarn Web license can be found at [https://gitlab.com/redwarn/redwarn-web/-/blob/master/LICENSE](https://gitlab.com/redwarn/redwarn-web/-/blob/master/LICENSE).