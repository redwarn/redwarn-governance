⟪ [Back](/README.md)
# Definition of Terms
The following terms are uniform throughout this repository and helps build an simpler view of technical wording.

* **RedWarn** refers to the anti-vandalism userscript, with a documentation page located at [Wikipedia:RedWarn](https://en.wikipedia.org/wiki/Wikipedia:RedWarn)
* **RedWarn Team** (or the **organization**) refers to the team behind the userscript, composed of the organizational members and other contributors that implement code changes, infrastructure, et cetera. A full list of members can be found at [Wikipedia:RedWarn/Team](https://en.wikipedia.org/wiki/Wikipedia:RedWarn/Team).
* **Majority** (in **majority vote**) refers to a 50% + 1 majority, calculated amongst all incumbent RedWarn team members.
